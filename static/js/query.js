
function _transTag(tag, node) {
    node.find('.'+tag).each(function(){
        var html = $(this).html();
        $(this).html('<span class="atag">{'+tag+'</span>'+
                     html+
                     '<span class="atag">'+tag+'}</span>')
    });
}


function transTags(src) {
    var s = $('<div>'+src+'</div>');

    _transTag('cc', s);
    _transTag('dd', s);
    _transTag('ii', s);
    _transTag('pp', s);
    _transTag('ss', s);
    _transTag('kk', s);
    _transTag('uu', s);
    _transTag('EleV', s);

    return s.html();
}
var mm= 0;
var Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
    var particleSystem

    var that = {
        init:function(system){
            particleSystem = system
            particleSystem.screenSize(canvas.width, canvas.height) 
            particleSystem.screenPadding(80) // leave an extra 80px of whitespace per side
            that.initMouseHandling()
        },
        
        redraw:function(){
            ctx.fillStyle = "white";
            ctx.fillRect (0,0, canvas.width, canvas.height);

            particleSystem.eachEdge (function (edge, pt1, pt2) {
                ctx.strokeStyle = "rgba(0,0,0, .333)";
                ctx.lineWidth = 1;
                ctx.beginPath ();
                ctx.moveTo (pt1.x, pt1.y);
                ctx.lineTo (pt2.x, pt2.y);
                ctx.stroke ();

                ctx.fillStyle = "black";
                ctx.font = 'italic 13px sans-serif';
                ctx.fillText (edge.data.name, (pt1.x + pt2.x) / 2, (pt1.y + pt2.y) / 2);
            });

            particleSystem.eachNode (function (node, pt) {
                var w = 10;
                ctx.fillStyle = "orange";
                ctx.fillRect (pt.x-w/2, pt.y-w/2, w,w);
                ctx.fillStyle = "black";
                ctx.font = 'italic 13px sans-serif';
                ctx.fillText (node.name, pt.x+8, pt.y+8);
            });
        },
        
        initMouseHandling:function(){
            // no-nonsense drag and drop (thanks springy.js)
            var dragged = null;

            // set up a handler object that will initially listen for mousedowns then
            // for moves and mouseups while dragging
            var handler = {
                clicked:function(e){
                    var pos = $(canvas).offset();
                    _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
                    dragged = particleSystem.nearest(_mouseP);

                    if (dragged && dragged.node !== null){
                        // while we're dragging, don't let physics move the node
                        dragged.node.fixed = true
                    }

                    $(canvas).bind('mousemove', handler.dragged)
                    $(window).bind('mouseup', handler.dropped)

                    return false
                },
                dragged:function(e){
                    var pos = $(canvas).offset();
                    var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

                    if (dragged && dragged.node !== null){
                        var p = particleSystem.fromScreen(s)
                        dragged.node.p = p
                    }

                    return false
                },

                dropped:function(e){
                    if (dragged===null || dragged.node===undefined) return
                    if (dragged.node !== null) dragged.node.fixed = false
                    dragged.node.tempMass = 1000
                    dragged = null
                    $(canvas).unbind('mousemove', handler.dragged)
                    $(window).unbind('mouseup', handler.dropped)
                    _mouseP = null
                    return false
                }
            }
            
            // start listening
            $(canvas).mousedown(handler.clicked);

        },
        
    }
    return that
}    

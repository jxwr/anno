/* generate position */
var maxLayer = 0;
var maxIndex = [1];

function DataNode() {
    this.name = '';
    this.pos = [-1,-1,-1,-1];
    this.infok = '';
    this.cate = '';
    this.children = [];

    this.infoElement = '';
    this.infoValue = '';
    this.defaultValue = -1;
    this.src = '';
    this.ref = '';
    this.L = '';

    this.R = [-1,-1,-1,-1]
    this.keyword = '';

    this.cateIndex = -1;
    this.cateSrcIndex = -1;
    this.cateInfoElementName = '';
    this.cateInfoElementValue = '';

    this.nodeIndex = -1;
    this.metaid = '';
}

function AnnoNode(node) {
    this.inner = node;
    this.name = node == null ? '根(Ke)' : node.text();
    this.keyword = '';
    this.children = [];
    this.pos = node == null ? [0,1,0,1] : null;
    this.nodeIndex = -1;
}

AnnoNode.prototype = {
    get keywords() {
        return this.infok.split(',')[14] || this.name;
    },

    get infok() {
        var tt = this.inner;
        if(tt == null) {
            return '0,1,0,1,KN,o,0,o,o,o,0,0,0,0,' + (DochNormal.Ku||"");
        }
        while(tagClass(tt) != 'kk' && tagClass(tt) != 'source') {
	    tt = tt.parent();
        }

        return tt.attr('pos')+','+tt.attr('infok') || false;
    },

    get cate() {
        if(!isElevSuite()) {
            return false;
        }

        var tt = this.inner;
        if(tt == null) {
            return false;
        }

        while(tagClass(tt) != 'ee' && tagClass(tt) != 'source') {
	    tt = tt.parent();
        }
        return tt.attr('cateIndex')+','+tt.attr('cate') || false;
    }
};

function AnnoTree(containerSel, canvasSel) {
    this.treeContainerSel = containerSel;
    this.treeCanvasSel = canvasSel;
    this.layers = [[new AnnoNode(null)]];
}

AnnoTree.renderByNode = function(root, treeCanvasSel) {
    var m = [20, 120, 20, 120],
    w = $(window).width() - m[1] - m[3],
    h = $(window).height() - m[0] - m[2],
    i = 0,
    root;

    console.log("root:");
    console.log(root);

    var tree = d3.layout.tree()
	.size([h, w]);

    var diagonal = d3.svg.diagonal()
	.projection(function(d) { return [d.y, d.x]; });

    var vis = d3.select(treeCanvasSel).append("svg:svg")
	.attr("width", w + m[1] + m[3])
	.attr("height", h + m[0] + m[2])
	.append("svg:g")
	.attr("transform", "translate(" + m[3] + "," + m[0] + ")");

    function callFill(json) {
        // console.log(json);
	root = json;
	root.x0 = h / 2;
	root.y0 = 0;

	function toggleAll(d) {
	    if (d.children) {
		d.children.forEach(toggleAll);
		toggle(d);
	    }
	}

	// Initialize the display to show a few nodes.
	//root.children.forEach(toggleAll);
	update(root);
    }

    function update(source) {
	var duration = d3.event && d3.event.altKey ? 5000 : 500;

	// Compute the new tree layout.
	var nodes = tree.nodes(root).reverse();
        var maxDepth = 0;

	nodes.forEach(function(d) { maxDepth = maxDepth < d.depth ? d.depth : maxDepth; });
	// Normalize for fixed-depth.
	nodes.forEach(function(d) { d.y = d.depth*w/maxDepth; });

	// Update the nodes…
	var node = vis.selectAll("g.node")
	    .data(nodes, function(d) { return d.id || (d.id = ++i); });

	// Enter any new nodes at the parent's previous position.
	var nodeEnter = node.enter().append("svg:g")
	    .attr("class", "node")
	    .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
	    .on("click", function(d) { toggle(d); update(d); });

	nodeEnter.append("svg:circle")
	    .attr("r", 1e-6)
	    .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

        // name
	nodeEnter.append("svg:text")
            .attr("transform", "rotate(-10)")
	    .attr("x", function(d) { return 0; })
            .attr("y", function(d) { return -15; })
	    .attr("dy", ".35em")
	    .attr("text-anchor", function(d) { return 'middle'; })
	    .text(function(d) { 
                return d.keywords || d.name
            })
	    .style("fill-opacity", 1e-6);

        // position
	nodeEnter.append("svg:text")
	    .attr("x", function(d) { return 0; })
            .attr("y", function(d) { return 15; })
	    .attr("dy", ".35em")
	    .attr("text-anchor", function(d) { return 'middle'; })
	    .text(function(d) { return String(d.pos); })
	    .style("fill-opacity", 1);

	// Transition nodes to their new position.
	var nodeUpdate = node.transition()
	    .duration(duration)
	    .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

	nodeUpdate.select("circle")
	    .attr("r", 4.5)
	    .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

	nodeUpdate.select("text")
	    .style("fill-opacity", 1);

	// Transition exiting nodes to the parent's new position.
	var nodeExit = node.exit().transition()
	    .duration(duration)
	    .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
	    .remove();

	nodeExit.select("circle")
	    .attr("r", 1e-6);

	nodeExit.select("text")
	    .style("fill-opacity", 1e-6);

	// Update the links…
	var link = vis.selectAll("path.link")
	    .data(tree.links(nodes), function(d) { return d.target.id; });

	// Enter any new links at the parent's previous position.
	link.enter().insert("svg:path", "g")
	    .attr("class", "link")
	    .attr("d", function(d) {
		var o = {x: source.x0, y: source.y0};
		return diagonal({source: o, target: o});
	    })
	    .transition()
	    .duration(duration)
	    .attr("d", diagonal);

	// Transition links to their new position.
	link.transition()
	    .duration(duration)
	    .attr("d", diagonal);

	// Transition exiting nodes to the parent's new position.
	link.exit().transition()
	    .duration(duration)
	    .attr("d", function(d) {
		var o = {x: source.x, y: source.y};
		return diagonal({source: o, target: o});
	    })
	    .remove();

	// Stash the old positions for transition.
	nodes.forEach(function(d) {
	    d.x0 = d.x;
	    d.y0 = d.y;
	});
    }

    // Toggle children.
    function toggle(d) {
	console.log()
	if (d.children) {
	    d._children = d.children;
	    d.children = null;
	} else {
	    d.children = d._children;
	    d._children = null;
	}
    }

    callFill(root);
}

AnnoTree.prototype = {
    get maxLayer() {
	// from 0
	return this.layers.length - 1;
    },

    // from pos to node
    fromPos: function(pos) {
	var layer = this.layers[pos[0]];
	var annoNode = layer[pos[1]-1]
	return annoNode;
    },

    addAnnoNode: function(ppos, node, nodeIndex) {
	var parent = this.fromPos(ppos);
	var annoNode = new AnnoNode(node);

        annoNode.nodeIndex = nodeIndex;

	parent.children.push(annoNode);
	
	// if layer of parent == maxlayer, add a new layer
	if(ppos[0] == this.maxLayer) {
	    this.layers.push([annoNode]);
	}
	// else just push the node to the next layer
	else if(ppos[0] < this.maxLayer) {
	    this.layers[ppos[0]+1].push(annoNode);
	}

	annoNode.pos = [ppos[0], ppos[1], ppos[0]+1, this.layers[ppos[0]+1].length];
	if(annoNode.inner) 
	    getParentNode(annoNode.inner, 'kk').attr('pos', ''+annoNode.pos);
	console.log(annoNode.pos);

	return [ ppos[0], ppos[1], ppos[0]+1, this.layers[ppos[0]+1].length ];
    },

    deleteAnnoNodeByPos: function(pos) {
	var annoNode = this.fromPos(pos);
	this.deleteAnnoNode(annoNode);
    },

    _deleteAnnoNode: function(posArray, i) {
	var theNode = this.fromPos(posArray[i]);
	var children = theNode.children

	for(var j = 0; j < children.length; j++) {
	    children[j].parent = null;
	}
		
	theNode.parent.children = _.without(children, [theNode]);
	var nodes = this.layers[posArray[i][0]];
	this.layers[posArray[i][0]] = _.without(nodes, [theNode]);
    },

    deleteAnnoNode: function(annoNode) {
	var posArray = this.enumNodePos();

	for(var i = 0; i < posArray.length; i++) {
	    if(this.fromPos(posArray[i]) == annoNode) {
		this._deleteAnnoNode(posArray, i);
	    }
	}
    },

    deleteAnnoNodeByInner: function(node) {
	var posArray = this.enumNodePos();
	var theNode = null;

	for(var i = 0; i < posArray.length; i++) {
	    if(this.fromPos(posArray[i]).node.is(node)) {
		this._deleteAnnoNode(posArray, i);
	    }
	}
    },

    // enumerate all nodes for selection
    enumNodePos: function() {
	var posArray = [];

	for(var i = 0; i <= this.maxLayer; i++) {
	    for(var j = 1; j <= this.layers[i].length; j++) {
		// console.log([i, j]);
		posArray.push([i, j]);
	    }
	}

	return posArray;
    },

    traverseByLayers: function(fn) {
	for(var i in this.layers) {
//	    console.log('layer ' + i + ':');
	    var layer = this.layers[i];

	    for(var j in layer) {
		var annoNode = layer[j];
		fn(annoNode, i, j);
	    }
	}
    },

    renderWithD3: function() {
        $(this.treeCanvasSel).html('');
        this.genCateIndex();
        this.genTreeIndex();

        AnnoTree.renderByNode(this.fromPos([0, 1]), this.treeCanvasSel);
    },

    toJsonTree: function() {
        this.genCateIndex();
        this.genTreeIndex();

        var root = this.fromPos([0,1])
        var tree = this._toJsonTree(root)

        console.log(tree);
        return tree;
    },

    _toJsonTree: function(node) {
        var tree = new DataNode();
        tree.name = node.name;
        tree.pos = node.pos;
        tree.nodeIndex = node.nodeIndex;
        tree.infok = node.infok;
        tree.cate = node.cate;
        tree.children = [];

        if(node.infok) {
            infokProps = node.infok.split(',');

            if(infokProps.length >= 15) {
                tree.infoElement = infokProps[4];
                tree.infoValue = infokProps[5];
                tree.defaultValue = parseInt(infokProps[6]);
                tree.src = infokProps[7];
                tree.ref = infokProps[8];
                tree.L = infokProps[9];

                tree.R = [parseInt(infokProps[10]),
                          parseInt(infokProps[11]),
                          parseInt(infokProps[12]),
                          parseInt(infokProps[13])];
                tree.keyword = infokProps[14];
            }
            else {
                if(node.inner) setCurrent(node.inner);
                alert('InfoK信息不完整');
                return tree;
            }
        }
        else {
            if(node.inner) setCurrent(node.inner);
            alert('InfoK信息未填写');
            return tree;
        }
        
        // Cate
        if(node.cate) {
            var cateProps = node.cate.split(',');
            
            if(cateProps.length == 4) {
                tree.cateIndex = parseInt(cateProps[0]);
                tree.cateSrcIndex = parseInt(cateProps[1]);
                tree.cateInfoElementName = cateProps[2];
                tree.cateInfoElementValue = cateProps[3];
            }
            else {
                if(node.inner) setCurrent(node.inner);
                alert('Cate信息不完整');
                return tree;
            }
        }

        var children = node.children;

        for(var i = 0; i < children.length; i++) {
            var childTree = this._toJsonTree(children[i]);
            tree.children.push(childTree);
        }

        return tree;
    },

    genCateIndex: function() {
        if(!isElevSuite()) 
            return;

        $('.source .ee').each(function(index) {
            $(this).attr('cateIndex', index+1);
        });
    },

    _genNodeIndex: function() {
        $('.source '+finalTagClass()).each(function(index) {
            $(this).attr('nodeIndex', index);
        });
    },

    genTreeIndex: function() {
        this._genNodeIndex();

        this.layers = [[new AnnoNode(null)]];
        var node = $(this.treeContainerSel);

        return this._genTreeIndex(node, -1, 0);
    },
    
    _genTreeIndex: function(node, ppos_x, ppos_y) {
        var uls = node.children('ul');

        for(var i = 0; i < uls.length; i++) {
            var lis = $(uls[i]).children('li');
            var idx = 1;

            if(lis.length == 0)
                return;
            for(var j = 0; j < lis.length; j++) {
                if(ppos_x == -1) {
                    // not need insert root node
                }
                else {
                    var nodeid = $(lis[j]).attr('nodeid');
                    var node = nodeMap[nodeid];

                    this.addAnnoNode([ppos_x, ppos_y], node, parseInt(node.attr('nodeIndex')));
                    node.removeAttr('nodeIndex');
                }

                this._genTreeIndex($(lis[j]), ppos_x+1, idx);
                idx++;
            }
        }
    }
}

//-----------------------------------
var idx = 1234;
// nodes in node container jstree
var nodeMap = {};

function selectTreeNode(node) {
    $('#node-container').jstree('deselect_all');
    $('#tree-container').jstree('deselect_all');

    for(var i in nodeMap) {
        if(nodeMap[i].is(node)) {
            $('#node-container').jstree('select_node', '[nodeid='+i+']');
            $('#tree-container').jstree('select_node', '[nodeid='+i+']');
        }
    }
}

function updateFinalNodes() {
    // get finalnodes of source
    var finalNodes = []; 
    $('.source '+finalTagClass()).each(function(){
        finalNodes.push($(this));
    });

    // get finalnodes of tree(node list and node tree)
    var nodesInTree = [];
    $('#node-container [nodeid]').each(function(){
        nodesInTree.push($(this));
    });
    $('#tree-container [nodeid]').each(function(){
        nodesInTree.push($(this));
    });

    var newNodeMap = {}
    for(var i = 0; i < finalNodes.length; i++) {
        var isNew = true;

        for(var j in nodeMap) {
            if(finalNodes[i].is(nodeMap[j])) {
                newNodeMap[j] = finalNodes[i];
                delete nodeMap[j];
                isNew = false
            }
        }

        if(isNew) {
            console.log('create');
            $('#node-container').jstree('create', -1, 'last', 
                                        {attr: {'nodeid': idx}, 
                                         data:finalNodes[i].text()},
                                        false, true);
            newNodeMap[idx++] = finalNodes[i];
        }
    }

    console.log(newNodeMap);
    console.log(nodeMap);

    for(var d in nodeMap) {
        // detach children nodes and insert them 
        // to root of node-container, then delete
        // the node change to 'NOT FINAL STATE'
        var pendingNodes;

        pendingNodes = $('#node-container [nodeid='+d+']').children('ul').children('li');
        pendingNodes.detach();
        // console.log(pendingNodes);
        $('#node-container').children('ul').append(pendingNodes);

        pendingNodes = $('#tree-container [nodeid='+d+']').children('ul').children('li');
        pendingNodes.detach();
        $('#node-container').children('ul').append(pendingNodes);

        $('#node-container, #tree-container').jstree('remove', '[nodeid='+d+']');
    }

    nodeMap = newNodeMap;
}

function renderTree() {
    var tree = new AnnoTree('#tree-container', '#tree-canvas');
    tree.renderWithD3();
    tree.toJsonTree();
}

function initTree() {
    $('#node-container, #tree-container').jstree({
        ui: {select_limit: 1},
        plugins: ['themes', 'html_data', 'dnd', 'crrm', 'ui']
    });

    $('#node-container, #tree-container').bind(
        'select_node.jstree', 
        function(e, data){
            var last = $(this).find('.jstree-clicked').parent('li');
            for(var i in nodeMap) {
                if(i == last.attr('nodeid')) {
                    setCurrent(nodeMap[i], true);
                    break;
                }
            }
        });
}

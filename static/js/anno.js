/* annotate */

// doc head
var isDocHSaved = false;
var DocH = {}
var isDochNormalSaved = false;
var DochNormal = {}

// tag
var tagSuite = tagSuite1;
var pos = null;
var current = null;

// video
var videoAPI = null;
var hasVideo = false;
var currentTime = -1;
var videoStartTime = -1;
var videoEndTime = -1;
var videoStopTimeout = null;

function isElevSuite() {
    return tagSuite == tagSuite1;
}

function finalTag() {
    return tagSuite[tagSuite.length-1];
}

function finalTagClass() {
    return '.' + tagSuite[tagSuite.length-1];
}

// dirty hack, if selectTreeNode everytime
// select_node.jstree will be stack overflow
// fix this
function setCurrent(node, ignoreTree) {
    current = node;
    selectNode(node[0]);

    if(!ignoreTree)
        selectTreeNode($(node[0]));
}

function log(msg, clear) {
    console.log(msg);
}

function showErrorMessage(msg) {
    $('#error-message').text(msg);
    $('#error-message').show();
}

function showMessage(msg) {
    $('#message').text(msg);
    $('#message').show();
}

function InputState() {
    this.isClick = false;
    this.enclosure = null;
    this.range = getRange();

    var range = this.range;
    var start = $(range.startContainer).parent();
    var end = $(range.endContainer).parent();

    if(start[0].tagName != end[0].tagName) {
	showErrorMessage('选择区域不在某个标签内');
	return;
    }
    else {
	enclosure = start;

	if(enclosure.hasClass('source') && range.startOffset == range.endOffset) {
	    showErrorMessage('先选择一段文本');
	    return;
	}

	if(range.startOffset == range.endOffset) {
	    this.isClick = true;
	}

	if($(range.cloneContents()).children('span').length > 0) {
	    showErrorMessage('选择区域内已包含标签');
	    return;
	}

	this.enclosure = enclosure;

	for(var i in tagSuite) {
	    var tag = tagSuite[i];
	    if(enclosure.hasClass(tag)) {
		log('State IN:' + tag);
		this.enclosure.name = tag;
		if(this.isClick)
		    showMessage('已选中<' + tag + '>');
		else
		    showMessage('已选中<' + tag + '>内的文本');
		break;
	    }
	}
    }
}

function tagClass(node) {
    for(var i = 0; i < tagSuite.length; i++) {
	if(node.hasClass(tagSuite[i])) {
	    return tagSuite[i];
	}
    }
    return null;
}

function tagSlice(a, b) {
    var slice = [];
    var start = tagSuite.indexOf(a) + 1;
    var end = tagSuite.indexOf(b) + 1;

    for(var i = start; i < end; i++) {
	slice.push(tagSuite[i])
    }

    return slice;
}

function addTag(event) {
    var state = new InputState(); // new State again
    log('TAG in: ' + state.enclosure.name);
    log('TAG on: ' + tagClass($(this)));

    var tags = tagSlice(state.enclosure.name, tagClass($(this)));
    log('to tag:[' + tags + ']');

    var range = state.range;
    var inner = tags.pop();
    var spanTag = document.createElement('span');

    spanTag.className = inner;
    range.surroundContents(spanTag);

    spanTag = $(spanTag);
    current = spanTag;
    
    while(tags.length > 0) {
	var t = document.createElement('span');
	t.className = tags.pop();
	spanTag.wrap(t);
	spanTag = spanTag.parent();
	// log('add tag: ' + t.className);
    }

    // select this range
    selectRange(range);

    // rescan final nodes
    updateFinalNodes();

    // syn node selection 
    if(spanTag.find(finalTagClass()).length > 0) {
        setCurrent(spanTag.find(finalTagClass()));
    }

    update();
    event.stopPropagation();
}

function getTextNode(node) {
    return node.contents().filter(function(){
	return (this.nodeType == Node.TEXT_NODE) && $(this).text() != '';
    });
}

function deleteTag(event) {
    if(current) {
	var parent = current.parent();

	current.find('span').andSelf().each(function(){
	    var textNode = getTextNode($(this));
	    textNode.unwrap();
	});

	if(!parent.hasClass(tagSuite[0])) {
	    current = parent;
	    selectNode(parent[0]);
	}
	else
	    current = null;
    }

    // rescan final nodes
    updateFinalNodes();

    update();
    event.stopPropagation();
}

function updateVideoPlay() {
    var st = getParentNode($(current), 'kk').attr('startTime');
    var et = getParentNode($(current), 'kk').attr('endTime');
    console.log(st+':'+et);

    $('#set-video .btn-play-video').hide();
    if(parseFloat(et) > parseFloat(st)) {
        $('#set-video .btn-play-video').show();
    }
}

function showVideo() {
    if(hasVideo) {
        console.log(currentTime);
        var st = getParentNode($(current), 'kk').attr('startTime') || '未设置';
        var et = getParentNode($(current), 'kk').attr('endTime') || '未设置';
        
        $('.video-start-time').text(st);
        $('.video-end-time').text(et);

        updateVideoPlay();
    }
}

function updateActions(state) {
    $('#actions').html('');
    $('#actions').prepend('<div class="btn-group tag"></div>');

    var n = tagSuite.indexOf(state.enclosure.name);

    if(n < 0) {
	$('#actions').append('<label class="label">无可用动作</label>');
	return;
    }

    for(var i = n+1; i < tagSuite.length; i++) {
	$('#actions .tag').append('<button class="btn btn-info '+tagSuite[i]+'">' + 
				  tagSuite[i] + 
				  '</button>');
    }

    if(current && state.enclosure.name != tagSuite[0]) {
	$('#actions').append('<div class="btn-group untag"></div>');
	$('#actions .untag').append('<button class="btn btn-info '+tagSuite[i]+'">删除' + 
				    state.enclosure.name + 
				    '</button>');
    }

    if(current && state.enclosure.name == finalTag()) {
	$('#actions').append('<button class="btn btn-info btn-infok">'
                             + '修改InfoK' +
			     '</button><br/>');

        if(isElevSuite()) {
            $('#actions').append('<br/><button class="btn btn-info btn-cate">'
                                 + '修改Cate' +
			         '</button>');
        }

        if(hasVideo) {
            $('#actions').append('<br/><button class="btn btn-info btn-set-video">关联视频</button>');
        }
    }

    var tagBtns = $('#actions .tag button');

    tagBtns.hover(
	function(){
	    for(var i = 0; i < tagBtns.length; i++) {
		var btn = $(tagBtns[i]);
		btn.removeClass('btn-info');
		btn.addClass('btn-warning');

		if(btn.is($(this))) {
		    break;
		}
	    }
	},
	function(){
	    tagBtns.each(function(){
		$(this).removeClass('btn-warning');
		$(this).addClass('btn-info');
	    });
	}
    );

    tagBtns.click(addTag);

    $('#actions .btn-set-video').click(function(){
        showVideo();
        $('#dialog-set-video').dialog('open');
    });

    $('#actions .untag button').click(deleteTag);

    $('#actions .btn-infok').click(function(){
        showInfoK();
        $('#dialog-infok').dialog('open');
    });

    $('#actions .btn-cate').click(function(){
        showCate();
        $('#dialog-cate').dialog('open');
    });
}

function reset() {
    $('#error-message').text('');
    $('#error-message').hide();
    
    $('#message').text('');
    $('#message').hide();
    
    $('#actions').html('');
}

function checkRule() {
    var textNodes;

    textNodes = getTextNode($('.source'));
    console.log(textNodes);

    if(textNodes.length > 0) {
        for(var i = 0; i < textNodes.length; i++) {
            if(/^\s+$/.exec(textNodes[i].textContent) == null) {
	        selectNode(textNodes[i]);
	        update();
	        showErrorMessage('source标记未完成');
	        return false;
            }
        }
    }
    
    for(var i = 1; i < tagSuite.length-1; i++) {
	t = tagSuite[i];
	textNodes = getTextNode($('.source .' + t))

	if(textNodes == null)
	    break;

	if(textNodes.length > 0) {
	    selectNode(textNodes[0]);
	    update();
	    showErrorMessage('标记未完成');
	    return false;
	}
    }

    if(!isDochNormalSaved) {
	showErrorMessage('DocH未生成');
        $('#dialog-doch-normal').dialog('open');
	return false;
    }

    if(!isDocHSaved) {
	showErrorMessage('详细DocH未生成');
        $('#dialog-doch').dialog('open');
	return false;
    }

    var pendingNodes = $('#node-container [nodeid]');
    if(pendingNodes.length > 0) {
        showErrorMessage('有节点未插入到树中');
        $('#dialog-node-container').dialog('open');
        return false
    }

    var pass = true;

    $('.source .kk').each(function(){
        var pos = $(this).attr('pos');
        var infok = $(this).attr('infok');
        
        if(typeof pos == 'undefined' || pos == false) {
	    showErrorMessage('索引未生成，察看图会自动生成索引');
            pass = false
            return false;
        }
        if(typeof infok == 'undefined' || infok == false) {
	    showErrorMessage('有节点未标注InfoK信息');
            var node = $(this).find(finalTagClass());
            setCurrent(node);
            pass = false;
            return false;
        }
    });

    if(!pass) return false;

    showMessage('标记完成');
    return true;
}

function saveDochNormal(event) {
    event.stopPropagation();
    event.preventDefault();

    $('#doch-normal .doch-input').each(function(){
        DochNormal[$(this).attr('name')] = $(this).val();
    });

    $('#doch-normal .doch-select').each(function(){
        DochNormal[$(this).attr('name')] = $(this).children('option:selected').text();
    });

    isDochNormalSaved = true;

    $('#doch-normal legend').html('DocH<small>（已保存）</small>');
    showMessage('保存成功');
    $('#dialog-doch-normal').dialog('close');
}

function saveDocH(event) {
    event.stopPropagation();
    event.preventDefault();

    $('#doch .doch-input').each(function(){
        DocH[$(this).attr('name')] = $(this).val();
    });

    $('#doch .doch-select').each(function(){
        DocH[$(this).attr('name')] = $(this).children('option:selected').text();
    });

    isDocHSaved = true;

    $('#doch legend').html('详细DocH<small>（已保存）</small>');
    showMessage('保存成功');
    $('#dialog-doch').dialog('close');
}

function saveCate(event) {
    event.stopPropagation();
    event.preventDefault();

    var info = $('#select-cate-info').attr('value');
    var element = $('#select-cate-element').attr('value');
    var srcIndex = $('#select-cate-src-index').attr('value');

    console.log([srcIndex, info, element]);

    var tt = current;

    if(tt) {
        while(tagClass(tt) != 'ee') {
	    tt = tt.parent();
        }

        tt.attr('cate', ''+srcIndex+','+info+','+element);
    }

    showCate();
}

function saveInfoK(event) {
    var element = $('#infok-element option:selected').text();
    log('InfoK:' + element, true);

    var value = $('#infok-value option:selected').text();
    log('InfoK:' + value);

    var dft = $('#infok-default').val();
    log('InfoK:' + dft);

    var src = $('#infok-src option:selected').text();
    log('InfoK:' + src);

    var ref = $('#infok-ref option:selected').text();
    log('InfoK:' + ref);

    var l = $('#infok-l option:selected').text();
    log('InfoK:' + l);

    var r = $('#infok-r').val();
    log('InfoK:' + r);

    var keyword = $('#infok-keyword').val();
    log('InfoK:' + r);

    var tt = current;
    while(tagClass(tt) != 'kk') {
	tt = tt.parent();
    }

    tt.attr('infok', 
	    element+','+value+','+dft+','+src+','+ref+','+l+','+r+','+keyword);

    showInfoK();

    event.stopPropagation();
    event.preventDefault();
}

function _get_parent(name) {
    return getParentNode(current, name);
}

function getParentNode(node, name) {
    var tt = node;

    while(tt && tagClass(tt) != name && tagClass(tt) != 'source') {
	if(!tt)
	    break;
	tt = tt.parent();
    }

    return tt;
}

function showCate(event) {
    if(!isElevSuite()) {
        return;
    }

    var tt = _get_parent('ee');

    if(tt) {
        var cate = tt.attr('cate');
        var props;

        if(cate) {
            $('#cate legend').html('Cate:<small>(已保存)</small>');
            props = cate.split(',');

	    function set_select(prop, index) {
	        $('#cate-'+prop+' option').map(function(){
		    if($(this).text() == props[index]) {
		        $(this).attr('selected', 'selected');
		    }
	        });
	    }

            $('#select-cate-src-index').attr('value', props[0]);
            $('#select-cate-info').attr('value', props[1]);
            $('#select-cate-element').attr('value', props[2]);
        }
        else {
            $('#cate legend').html('Cate:<small>(未保存)<small>');
            $('#select-cate-src-index').attr('value', '0');
            $('#select-cate-info').attr('value', 'p');
            $('#select-cate-element').attr('value', 'S');
        }

        var abstr = tt.find(finalTagClass()).text();
        abstr = abstr.length > 20 ? abstr.substring(0, 20) + '...' : abstr;
            
        $('#cate legend').append('<small>&nbsp;' + abstr + '</small>');
        $('#cate legend').append('<br>');
        $('#cate legend').append(cate||'');
    }
}

function showInfoK() {
    var tt = _get_parent('kk');

    if(tt) {
	var infok = tt.attr('infok');
	var props;
        var str = '';

	if(infok) {
	    $('#infok legend').html('InfoK:<small>(已保存)<small>');
            str = '<small>'+infok+'</small>';
	    props = infok.split(',');
	}
	else {
	    $('#infok legend').html('InfoK:<small>(未保存)<small>');
	    props = ['信息元素','信息点值','0','信息源','被引用者','L信息','0','0','0','0',''];
	}

        var abstr = tt.find(finalTagClass()).text();
        abstr = abstr.length > 20 ? abstr.substring(0, 20) + '...' : abstr;
        
        $('#infok legend').append('<small>&nbsp;' + abstr + '</small>');
        $('#infok legend').append('<br>');
        $('#infok legend').append(str);

	function set_select(prop, index) {
	    $('#infok-'+prop+' option').map(function(){
		if($(this).text() == props[index]) {
		    $(this).attr('selected', 'selected');
		}
	    });
	}

	function set_value(prop, value) {
	    $('#infok-'+prop).val(value);
	}

	set_select('element', 0);
	set_select('value', 1);
	set_value('default', props[2]);
	set_select('src', 3);
	set_select('ref', 4);
	set_select('l', 5);
	set_value('r', props.slice(6,10).join(','));
	set_value('keyword', props[10]);
    }
    
    $('#infok').show();
}

function updatePath(state) {
    $('#cur-pos').html('');

    if(current == null) { 
	$('#cur-pos').html('未选择路径');
	return;
    }

    var tagName = tagClass(current);
    var n = tagSuite.indexOf(tagName);

    if(n < 1) {
	return;
    }

    $('#cur-pos').prepend('<div class="btn-group"></div>');

    for(var i = 1; i <= n; i++) {
	var t = tagSuite[i];
	var btn = t == tagName ? 'btn-warning' : 'btn-success';
	var style = btn + ' disabled';

	$('#cur-pos div').append('<button class="btn '+style+' path '+t+'">' +  t + '</button>');
    }
}

function update() {
    reset();

    var state = new InputState();
    var enc = state.enclosure;

    if(enc) {
	if(state.isClick && enc.name) {
	    log('SELECT: ' + enc.name);
	    setCurrent(enc);
            showCate();
            showInfoK();
            showVideo();
	}

	if(enc.name == tagSuite[0])
	    current = null;

	updateActions(state);
    }
    else {
	current = null;
	$('#actions').html('无可用动作');
    }

    updatePath();
}

function adjustSize() {
    // ajust text-body width
    var w = parseInt($('#action-panel').outerWidth(true));

    var pad = parseInt($('#text-body').css('padding-left')) || 0;
    var mar = parseInt($('#text-body').css('margin-left')) || 0;

    var nw = parseInt($(document).width()) - 2*pad - 2*mar - w - 20;

    $('#text-body').css('width', nw);
}

function initDebugDialog() {
    $('#dialog-debug').dialog({
        autoOpen: false,
        title: 'Debug',
        width: 400,
        height: 400
    });

    $('#btn-debug').click(function(){
        $('#dialog-debug').dialog('open');
    });
}

function initInfokDialog() {
    $('#dialog-infok').dialog({
        autoOpen: false,
        title: 'InfoK',
        width: 500,
        height: 380
    });

    $('#btn-infok').click(function(){
        $('#dialog-infok').dialog('open');
    });

    $('#btn-save-infok').click(saveInfoK);
}

function initVideoDialog() {
    $('#dialog-set-video').dialog({
        autoOpen: false,
        title: '视频标注',
        width: 500,
        height: 350
    });

    $('#btn-set-video').click(function(){
        $('#dialog-set-video').dialog('open');
    });

    $('.btn-start-time').click(function(){
        if(currentTime > 0) {
            console.log('starttime:'+currentTime);

            var et = getParentNode($(current), 'kk').attr('endTime');

            if(currentTime <= parseFloat(et)) {
                showErrorMessage('开始时间不能小于结束时间');
                return;
            }

            getParentNode($(current), 'kk').attr('startTime', currentTime);
            showVideo();
        }
    });

    $('.btn-end-time').click(function(){
        if(currentTime > 0) {
            console.log('endtime:'+currentTime);

            var st = getParentNode($(current), 'kk').attr('startTime');

            if(currentTime <= parseFloat(st)) {
                showErrorMessage('开始时间不能小于结束时间');
                return;
            }

            getParentNode($(current), 'kk').attr('endTime', currentTime);
            showVideo();
        }
    });

    $('#set-video .btn-play-video').click(function(){
        var st = getParentNode($(current), 'kk').attr('startTime');
        var et = getParentNode($(current), 'kk').attr('endTime');
        videoAPI.seek(st);
        videoAPI.video.time = st;
        console.log((parseFloat(et)-parseFloat(st))*1000);
        videoStopTimeOut = setTimeout(function(){videoAPI.pause();}, 
                                      (parseFloat(et)-parseFloat(st))*1000);

        if(videoAPI.paused) {
            videoAPI.toggle();
        }
    });

    $('#btn-save-video-info').click(function(){
        $('#video-dialog-status').text('');
        var modelType = $('#video-model-type').val();
        var videoClass = $('#video-class').val();
        console.log(videoClass);
        getParentNode($(current), 'kk').attr('model-type', modelType);
        getParentNode($(current), 'kk').attr('video-class', videoClass);

        var st = getParentNode($(current), 'kk').attr('startTime');
        var et = getParentNode($(current), 'kk').attr('endTime');

        if(isNaN(parseFloat(st)) || isNaN(parseFloat(et)) || parseFloat(st) >= parseFloat(et)) {
            $('#video-dialog-status').text('视频开始时间或结束时间标记不正确');
        }
        else {
            $('#video-dialog-status').text('已保存');
        }
    });
}

function initTreeDialog() {
    // jstree node list
    $('#dialog-node-container').dialog({
        autoOpen: false,
        title: '待插入节点',
        position: ['right', 'bottom'],
        width: 400,
        height: 300
    });

    var nw = $(window).width() - 420;

    $('#btn-node-container').click(function(){
        $('#dialog-node-container').dialog('open');
        updateFinalNodes();
    });

    // jstree tree
    $('#dialog-tree-container').dialog({
        autoOpen: false,
        title: '树视图',
        position: ['left', 'bottom'],
        width: nw,
        height: 300
    });

    $('#btn-tree-container').click(function(){
        $('#dialog-tree-container').dialog('open');
        updateFinalNodes();
    });

    // graphic
    $('#dialog-tree-canvas').dialog({
        autoOpen: false,
        title: '图视图',
        position: ['left', 'top'],
        width: $(window).width(),
        height: $(window).height()-20
    });
    
    $('#btn-tree-canvas').click(function(){
        renderTree();
        $('#dialog-tree-canvas').dialog('open');
    });
}

function translate(type) {
    var dummy = $('.source').clone();

    dummy.find('.kk').each(function(){
        $this = $(this);
        var pos = $this.attr('pos');
        var infok = $this.attr('infok');

        /*if($this.attr('model-type')) {
            infok += ',' + $this.attr('model-type') + ',' + $this.attr('video-class');
        }*/

        $this.attr('infok', pos+','+infok);
        $this.removeAttr('pos');

        //$this.removeAttr('model-type');
        //$this.removeAttr('video-class');
    });

    dummy.find('.ee').each(function(){
        $this = $(this);
        var idx = $this.attr('cateIndex');
        var cate = $this.attr('cate');
        $this.attr('cate', idx+','+cate);
        $this.removeAttr('cateIndex');
    });

    var content = dummy.html();
    console.log($('#result').content);
    
    var trans = new Trans(content, DochNormal, DocH);

    return trans.translate(type);
}

function storeResult() {
    if(!checkRule()) {
	alert('标注不完整');
	return;
    }

    var tree = new AnnoTree('#tree-container', '#tree-canvas');

    // add tooltip for element of kk
    $('.kk[infok]').each(function(index, elem){
        var e = $(elem);
        e.wrap('<a class="hover-infok" href="#" rel="tooltip" title="infok='+e.attr('pos')+','+e.attr('infok')+'">');
    });

    // add video display info
    $('.kk[infok]').each(function(info, elem) {
    });

    var taggedSrc = $('.source').html();

    $('.kk[infok]').unwrap();
    
    var result = {
        doch: DochNormal,
        dochLong: DocH,
        xmlDoc: translate('Discourse'),
        xmlDocH: translate('DocH'),
        taggedSrc: taggedSrc,
        tree: tree.toJsonTree()
    };

    var metaid = $(this).attr('metaid');
    $.post('/store', 
           {metaid: metaid, 
            annoTree: JSON.stringify(result)}, 
           function(data){
               if(data) {
                   console.log('------------------upload result');
                   console.log(data);
                   alert('保存成功');
               }
               else {
                   alert('保存失败');
               }
           },
           'json');
}

function getTreeDraft() {
    var node_html = $('#node-container').html();
    var tree_html = $('#tree-container').html();

    return {idx: idx,
            //nodeMap: nodeMap,
            node_html: node_html,
            tree_html: tree_html
           }
}

function saveDraft() {
    tree_draft = getTreeDraft();

    $.post('/personal/mine', 
           {action: 'draft',
            body_draft: $('.source').html(),
            tree_draft: JSON.stringify(tree_draft)},
           function(){
               alert('已保存');
           });
}

function initButtons() {
    $('#btn-check').click(checkRule);
    $('#btn-store').click(storeResult);

    $('#btn-change-doc').click(function(){
        $('#dialog-upload-src').dialog('open');
    });

    $('#btn-gen').click(function(){
	if(!checkRule()) {
	    alert('标注不完整');
	    return;
	}

        var result = translate('Discourse');
	log(result, true);

        $('#result').text(result);

        $('#dialog-result').dialog('open');

	$(finalTagClass()).each(function(index, value){
	    console.log($(value).text());
	});
    });

    $('#btn-gen-doch').click(function(){
	if(!checkRule()) {
	    alert('标注不完整');
	    return;
	}

        var result = translate('DocH');
	log(result, true);

        $('#result').text(result);

        $('#dialog-result').dialog('open');

	$(finalTagClass()).each(function(index, value){
	    console.log($(value).text());
	});
    });

    $('#btn-save-draft').click(saveDraft);
}

function initCateDialog() {
    $('#dialog-cate').dialog({
        autoOpen: false,
        title: 'Cate',
        width: 600,
        height: 300
    });

    $('#btn-cate').click(function(){
        $('#dialog-cate').dialog('open');
    });

    $('#btn-save-cate').click(saveCate);
}


function initDochDialog() {
    $('#dialog-doch').dialog({
        autoOpen: false,
        title: '详细文档头',
        width: 700,
        height: 600
    });

    $('#btn-doch').click(function(){
        $('#dialog-doch').dialog('open');
    });

    $('#btn-save-doch').click(saveDocH);

    // normal doch
    $('#dialog-doch-normal').dialog({
        autoOpen: false,
        title: '文档头',
        width: 700,
        height: 600
    });

    $('#btn-doch-normal').click(function(){
        $('#dialog-doch-normal').dialog('open');
    });

    $('#btn-save-doch-normal').click(saveDochNormal);

    $('#btn-gen-doch').click(saveDochNormal);
}

function initActionDialog() {
    $('#dialog-actions').dialog({
        autoOpen: false,
        title: '可用动作',
        width: 400,
        height: 250
    });

    $('#btn-actions').click(function(){
        $('#dialog-actions').dialog('open');
    });
}

function initResultDialog() {
    $('#dialog-result').dialog({
        autoOpen: false,
        title: 'XML',
        width: 600,
        height: 400
    });
}

$(function(){
    reset();
    initTree();

    $(window).resize(adjustSize);
    adjustSize();

    $('#actions').html('无可用动作');

    initActionDialog();
    initTreeDialog();
    initInfokDialog();
    initResultDialog();
    initButtons();
    initDochDialog();
    initCateDialog();
    initVideoDialog();

    // main loop
    $('.source').mouseup(function(event){
	update();
	event.stopPropagation();
    });
});
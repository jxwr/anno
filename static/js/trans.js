
function Trans(source, doch, dochLong) {
    this.cursor = 0;
    this.source = String(source);
    this.doch = doch;
    this.dochLong = dochLong;
    this.stack = [];
    this.result = '';
}

const openRegexp = /^<span\sclass="([a-zEV]+)">/;
const openRegexpKK = /^<span\s+class="([a-zEV]+)"\s+infok="([^>]*)">/;

const openRegexpCate = /^<span\s+class="([a-zEV]+)"\s+cate="([^>]*)">/;

function _wrap(src, tag) {
    return '<'+tag+'>'+ src + '</'+tag+'>\n';
}

Trans.prototype = {
    get input() {
	return this.source.substring(this.cursor);
    },

    translate: function(type) {
        if(type == 'Discourse') {
            var head = this.translateDocH(type);
            var body = this.translateBody();

            console.log(head);
            return _wrap(head+body, 'Discource');
        }
        else if(type == 'DocH') {
            var head = this.translateDocH(type);
            return head;
        }
    },

    translateDocH: function(type) {
        var doch = type == 'DocH' ? this.dochLong : this.doch;

        function w(tag) {
            console.log(doch[tag]);
            return _wrap(doch[tag], tag);
        }

        var head;
        head = _wrap(this.doch['T'], 'T');
        head += '<Ke InfoK="0,1,0,1,KN,o,o,o,o,0,0,0,0,'+ this.doch['Ku'] +'">\n' + 
            _wrap(this.doch['Ku'], 'Ku') + '</Ke>';
        head = 
        head += _.reduce(_.map(['SN', 'CT', 'LG', 'ST'], w), 
                         function(d, e){return d+e}, '');

        var tmp = _.reduce(_.map(['lawyerA','lawyerB','lawyerC','judge',
                                  'interpreter','partyA','partyB','partyC',
                                  'testimonyA','testimonyB','testimonyC',
                                  'audience','authorwriter','quotesource',
                                  'recorder'], w),
                           function(d, e){return d+e}, '');
        head += _wrap(tmp, 'Pa');

        if(type == 'Discourse') {
            // no op
        }
        else if(type == 'DocH') {
            head += _.reduce(_.map(['casename','caselevel','casetime',
                                    'collector','collectplace','collecttime',
                                    'datalength','dataclass','original',
                                    'transcription','modified','infotagged',
                                    'transtagged','whotagged','otherlinks'], w),
                             function(d, e){return d+e}, '');
        }

        return _wrap(head, 'DocH');
    },

    translateBody: function() {
	while(true) {
	    if(this.cursor >= this.source.length-1)
		break;

	    var input = this.input
	    var ch = input[0]

	    switch(ch) {
	    case '<':
		var match;
		    
		if((match = /^<\/span>/.exec(input))) {
		    this.cursor += match[0].length;

		    var cls = this.stack.pop();
		    this.result += '</'+cls+'>';

		    break;
		}

		if((match = openRegexp.exec(input))) {
		    this.cursor += match[0].length;

		    var cls = match[1];
		    this.stack.push(cls);
		    this.result += '<'+cls+'>';

		    break;
		}

		if((match = openRegexpKK.exec(input))) {
		    this.cursor += match[0].length;

		    var cls = match[1];
		    this.stack.push(cls);
		    this.result += '<'+cls+' InfoK="' + match[2] + '">';

                    break;
		}

		if((match = openRegexpCate.exec(input))) {
		    this.cursor += match[0].length;

		    var cls = match[1];
		    this.stack.push(cls);
		    this.result += '<'+cls+' Cate="' + match[2] + '">';	

		    break;
		}

		alert('输入source错误');
		break;
	    default:
		this.result += ch;
		this.cursor++;
		break;
	    }
	}
	return this.result;
    }
}

function genPosition(node, deep) {
    var oneTree = node.children();

    var total = []
    for(var i in children) {
	total.concat(genPosition(children[i], deep+1));
    }
}
function getRangeObject(selectionObject) {
    if (selectionObject.getRangeAt)
        return selectionObject.getRangeAt(0);
    else { // Safari!
        var range = document.createRange();
        range.setStart(selectionObject.anchorNode,selectionObject.anchorOffset);
        range.setEnd(selectionObject.focusNode,selectionObject.focusOffset);
        return range;
    }
}

function getRange() {
    var selection;
    if(window.getSelection) {
	selection = window.getSelection();
    }
    if(document.selection) {
	selection = document.selection.createRange();
    }
    
    var range = getRangeObject(selection);
    
    return range;
}

function selectRange(range) {
    unselect();
    if(document.selection) {
	range.select();
    }
    else if(window.getSelection) {
	window.getSelection().addRange(range);
    }
}

function selectNode(node, append) {
    if(!append)
	unselect();

    if(document.selection) {
	var range = document.body.createTextRange();
	range.moveToElementText(node);
	range.select();
    }
    else if(window.getSelection) {
	var range = document.createRange();
	range.selectNode(node);
	window.getSelection().addRange(range);
    }
}

function unselect() 
{
    if (document.selection)
        document.selection.empty();
    else if (window.getSelection)
        window.getSelection().removeAllRanges();
} 

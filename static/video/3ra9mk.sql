create table if not exists user (
    user_id     int(11) auto_increment,
    email       varchar(80) default '' not null,
    password    char(32) default '' not null,
    registered_time     int(11),
    latestlogin_time    int(11),
    primary key(user_id)
) engine=InnoDB default charset=utf8;

create table if not exists profile (
    profile_id  int(11)         auto_increment,
    user_id     int(11)         default 0 not null,
    nickname    varchar(40)     default '' not null,
    photo0      varchar(100)    default '' not null,
    photo1      varchar(100)    default '' not null,
    photo2      varchar(100)    default '' not null,
    gender      tinyint     default 0 not null,
    birth       int(11),
    shengxiao   tinyint     default 0 not null,  
    constell    tinyint     default 0 not null,
    height      smallint    default 0 not null,
    sex_orien   tinyint     default 0 not null,
    weight      tinyint     default 0 not null,
    income      tinyint     default 0 not null,
    education   tinyint     default 0 not null,
    race        tinyint     default 0 not null,
    province    char(20)    default '' not null,
    city        char(20)    default '' not null,
    province_now    char(20)    default '' not null,
    city_now    char(20)    default '' not null,
    job         tinyint     default 0 not null,
    house_stat  tinyint     default 0 not null,
    car_stat    tinyint     default 0 not null,
    only_child  tinyint     default 0 not null,
    smoke_stat  tinyint     default 0 not null,
    drink_stat  tinyint     default 0 not null,
    primary key(profile_id)
) engine=InnoDB default charset=utf8;

create table if not exists tanbai (
    tanbai_id   int(11)     auto_increment,
    user_id     int(11)     not null,
    description text,
    content     text,
    primary key(tanbai_id)
) engine=InnoDB default charset=utf8;

-- Reserved the table name 'keyword' for future.
-- tag is keyword.
create table if not exists tag (
    tag_id          int(11) auto_increment,
    name            varchar(15) not null,
    created_time    int(11) not null,
    primary key(tag_id)
) engine=InnoDB default charset=utf8;

-- Many-to-many relation between User and Tag
create table if not exists user_tag (
    user_id         int(11) not null,
    tag_id          int(11) not null,
    primary key(user_id, tag_id),
    key ix_user_tag_user_id (user_id),
    key ix_user_tag_tag_id  (tag_id)
) engine=InnoDB default charset=utf8;

-- private message
create table if not exists message (
    message_id   int(11)     auto_increment,
    from_uid     int(11)     not null,
    to_uid       int(11)     not null,
    content      text,
    created_time int(11),
    primary key(message_id)
) engine=InnoDB default charset=utf8;

-- stared member, which should be counted out.
create table if not exists star_member_list (
    star_id      int         auto_increment,
    user_id      int(11)     not null,
    stared_time  int(11)
    primary key(star_id)
) engine=InnoDB default charset=utf8;

create table if not exists black_list (
    user_id     int(11),
    target_id   int(11),
    primary key(member_id, target_id)
) engine=InnoDB default charset=utf8;

-- follow a person or other things.
create table if not exists follow (
    user_id     int(11),
    target_id   int(11),
    target_type tinyint,
    primary key(user_id, target_id, target_type)
) engine=InnoDB default charset=utf8;

create table if not exists album (
    album_id        int         auto_increment,
    user_id         int(11)     not null,
    name            char(30)    default '' not null,
    created_time    int(11),
    description     text,
    -- amount of photos
    photos_num      int(11),
    primary key(album_id)
) engine=InnoDB default charset=utf8;

create table if not exists photo (
    photo_id        int(11)     auto_increment,
    album_id        int(11)     not null,
    title           char(30)    default '' not null,
    uploaded_time   int(11),
    description     text,
    file_path       varchar(100)   default '' not null,
    thumb_path      char(100)  default '' not null,
    primary key(photo_id)
) engine=InnoDB default charset=utf8;

create table if not exists shop (
    shop_id     int(11)     auto_increment,
    name        varchar(40) default '' not null,
    photo       varchar(80) default '' not null,
    description text,
    address     tinytext,
    telephone   tinytext,
    open_time   time        default '08:00:00' not null,
    close_time  time        default '20:00:00' not null,
    tips        text,
    type        tinyint     default 0 not null,
    created_time int(11)    not null,
    primary key(shop_id)
) engine=InnoDB default charset=utf8;

create table if not exists item (
    item_id     int(11)     auto_increment,
    shop_id     int(11)     not null,
    name        varchar(100)    default '' not null,
    tanbai_price    int     default 0 not null,
    real_price      int     default 0 not null,
    person_count    smallint    default 0 not null,
    buy_count       int     default 0 not null,
    fav_count       int     default 0 not null,
    -- like a menu to a food.
    content         text,
    photo      tinytext    not null,
    expired_time    int(11)    not null,
    created_time    int(11)    not null,
    primary key(item_id)
) engine=InnoDB default charset=utf8;

create table if not exists item_order (
    item_order_id   int(11) auto_increment,
    item_id         int(11) not null,
    item_num        tinyint not null,
    total_prices    int(11) not null,
    user_id         int(11) not null,
    date_req_id     int(11) not null,
    -- paid, unpaid, refunded
    status          tinyint default 0 not null,
    created_time    int(11) not null,
    primary key(item_order_id)
) engine=InnoDB default charset=utf8;

create table if not exists date_req (
    date_req_id     int(11) auto_increment,
    item_order_id   int(11) not null,
    from_uid        int(11) not null,
    to_uid          int(11) not null,
    message         int(11) not null,
    -- default 0 means uncertainly
    date_time       int(11) default 0 not null,
    created_time    int(11) not null,
) engine=InnoDB default charset=utf8;

create table if not exists favorite (
    favorite_id     int(11) auto_increment,
    user_id         int(11) not null,
    item_id         int(11) not null,
    created_time    int(11) not null,
    primary key(favorite_id)
) engine=InnoDB default charset=utf8;

-- Is there a requirement to follow or favor a shop?

-- comment system
create table if not exists comment (
    comment_id      int(11) auto_increment,
    user_id         int(11) not null,
    -- thread is a tanbai_page, a shop, a item, a album or a photo.
    thread_id       int(11) not null,
    thread_type     tinyint not null,
    body            text,
    created_time    int(11),
    primary key(comment_id)
) engine=InnoDB default charset=utf8;

-- economic system
-- tbb is the abbreviation of TanBai-Bi.
create table if not exists tbb_account (
    tbb_account_id  int(11) auto_increment,
    user_id         int(11) not null,
    amount          int(11) not null,
    primary key(tbb_account_id)
) engine=InnoDB default charset=utf8;

create table if not exists tbb_journal (
    tbb_journal_id  int(11) auto_increment,
    user_id         int(11) not null,
    action_type     tinyint default 0 not null,
    action_amount   int(11) not null,
    created_time    int(11) not null,
) engine=InnoDB default charset=utf8;



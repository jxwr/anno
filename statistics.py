# coding=utf-8

from bson.objectid import ObjectId
import pymongo as M
import json, math

info_element_values = ['KN', 'WT', 'WB', 'WF', 'WI',
                       'WP', 'WO', 'WN', 'WR', 'HW',
                       'WY', 'WE', 'WC', 'WA', 'WG' , 'WJ']

info_value_values = ['P', 'N', 'O']

src_ref_values = ['A', 'B', 'C', 'J', 'j', 'I', 'O', 
                  'S', 'P', 'Q', 'R', 'V', 'T', 'D',
                  'N', 'Z']
        
L_values = ['A', 'B', 'C', 'E', 'O', 'D',
            'a', 'b', 'c', 'e', 'o', 'd',
            'R', 'S', 'T', 'Y', 'Z', 'U']

import base
from base import BaseRequestHandler

class StatHandler(BaseRequestHandler):
    def stat_of_doc_list(self, metaid_list, node_cond={}):
        stat_info_element = {}
        stat_info_value = {}
        stat_src = {}
        stat_ref = {}
        stat_l = {}

        max_value = 1

        def init_stat_dict(lst, dct):
            for v in lst: dct[v] = 0

        init_stat_dict(info_element_values, stat_info_element)
        init_stat_dict(info_value_values, stat_info_value)
        init_stat_dict(src_ref_values, stat_src)
        init_stat_dict(src_ref_values, stat_ref)
        init_stat_dict(L_values, stat_l)

        for metaid in metaid_list:
            for v in info_element_values:
                cond = dict(node_cond)
                cond['infoElement'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_info_element[v] += count
                if stat_info_element[v] > max_value:
                    max_value = stat_info_element[v]

            for v in info_value_values:
                cond = dict(node_cond)
                cond['infoValue'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_info_value[v] += count
                if stat_info_value[v] > max_value:
                    max_value = stat_info_value[v]
        
            for v in src_ref_values:
                cond = dict(node_cond)
                cond['src'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_src[v] += count
                if stat_src[v] > max_value:
                    max_value = stat_src[v]

                cond = dict(node_cond)
                cond['ref'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_ref[v] += count
                if stat_ref[v] > max_value:
                    max_value = stat_ref[v]

            for v in L_values:
                cond = dict(node_cond)
                cond['L'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_l[v] += count
                if stat_l[v] > max_value:
                    max_value = stat_l[v]

        return {'info_element': stat_info_element,
                'info_value': stat_info_value,
                'src': stat_src,
                'ref': stat_ref,
                'L': stat_l}, max_value

    def cond_stat(self, meta_cond, node_cond={}):
        stat_info_element = {}
        stat_info_value = {}
        stat_src = {}
        stat_ref = {}
        stat_l = {}

        max_value = 1

        def init_stat_dict(lst, dct):
            for v in lst: dct[v] = 0

        init_stat_dict(info_element_values, stat_info_element)
        init_stat_dict(info_value_values, stat_info_value)
        init_stat_dict(src_ref_values, stat_src)
        init_stat_dict(src_ref_values, stat_ref)
        init_stat_dict(L_values, stat_l)

        meta_cond['status'] = base.MS_PASSED
        meta_cursor = self.db.meta.find(meta_cond)

        for meta in meta_cursor:
            metaid = str(meta['_id'])

            for v in info_element_values:
                cond = dict(node_cond)
                cond['infoElement'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_info_element[v] += count
                if stat_info_element[v] > max_value:
                    max_value = stat_info_element[v]

            for v in info_value_values:
                cond = dict(node_cond)
                cond['infoValue'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_info_value[v] += count
                if stat_info_value[v] > max_value:
                    max_value = stat_info_value[v]
        
            for v in src_ref_values:
                cond = dict(node_cond)
                cond['src'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_src[v] += count
                if stat_src[v] > max_value:
                    max_value = stat_src[v]

                cond = dict(node_cond)
                cond['ref'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_ref[v] += count
                if stat_ref[v] > max_value:
                    max_value = stat_ref[v]

            for v in L_values:
                cond = dict(node_cond)
                cond['L'] = v
                cond['metaid'] = metaid
                count = self.db.nodes.find(cond).count()

                stat_l[v] += count
                if stat_l[v] > max_value:
                    max_value = stat_l[v]
            
        return {'corpus': {},
                'info_element': stat_info_element,
                'info_value': stat_info_value,
                'src': stat_src,
                'ref': stat_ref,
                'L': stat_l}, max_value

    def global_stat(self):
        return self.db.global_stat.find_one()

    def compute_global_stat(self):
        stat_info_element = {}
        stat_info_value = {}
        stat_src = {}
        stat_ref = {}
        stat_l = {}

        max_value = 0
        for v in info_element_values:
            count = self.db.nodes.find({'infoElement': v}).count()
            stat_info_element[v] = count
            if count > max_value:
                max_value = count

        for v in info_value_values:
            count = self.db.nodes.find({'infoValue': v}).count()
            stat_info_value[v] = count
            if count > max_value:
                max_value = count
        
        for v in src_ref_values:
            count = self.db.nodes.find({'src': v}).count()
            stat_src[v] = count
            if count > max_value:
                max_value = count

            count = self.db.nodes.find({'ref': v}).count()
            stat_ref[v] = count
            if count > max_value:
                max_value = count

        for v in L_values:
            count = self.db.nodes.find({'L': v}).count()
            stat_l[v] = count
            if count > max_value:
                max_value = count

        
        corpus = self.db.meta.find({'corpus': {'$exists': True}})
        corpus_counter = {}
        
        for v in corpus:
            key = v['corpus']
            if type(key) != type(u'*'):
                key = str(int(key))
            corpus_counter[key] = corpus_counter.get(key, 0) + 1

        self.db.global_stat.remove()
        self.db.global_stat.insert({'stat':
                                        {'corpus': corpus_counter,
                                         'info_element': stat_info_element,
                                         'info_value': stat_info_value,
                                         'src': stat_src,
                                         'ref': stat_ref,
                                         'L': stat_l}, 
                                    'max_value':
                                        max_value})

    def get(self):
        self.post()

    def post(self):
        if self.get_argument('metaidList', False):
            metaid_list = self.get_argument('metaidList')

            # uniqifiy
            stat, max_value = self.stat_of_doc_list(list(set(metaid_list.split(','))))
            self.render('_stat_result.html',
                        stat=stat, 
                        is_global=False,
                        max_value=max_value)
        elif self.get_argument('form.submit', False):
            pos0 = self.get_argument('pos0', '*')
            pos1 = self.get_argument('pos1', '*')
            pos2 = self.get_argument('pos2', '*')
            pos3 = self.get_argument('pos3', '*')
            info_element = self.get_argument('infoElement', '*')
            info_value = self.get_argument('infoValue', '*')
            default_value = self.get_argument('defaultValue', '*')
            src = self.get_argument('src', '*')
            ref = self.get_argument('ref', '*')
            l = self.get_argument('L', '*')
            r0 = self.get_argument('R0', '*')
            r1 = self.get_argument('R1', '*')
            r2 = self.get_argument('R2', '*')
            r3 = self.get_argument('R3', '*')
            keyword = self.get_argument('keyword', '*')
            cate_index = self.get_argument('cateIndex', '*')
            cate_src_index = self.get_argument('cateSrcIndex', '*')
            cate_info_element_name = self.get_argument('cateInfoElementName', '*')
            cate_info_element_value = self.get_argument('cateInfoElementValue', '*')

            # meta conditions
            meta_cond = {}
            def set_meta_cond_str(name):
                if self.get_argument(name, None) is not None:
                    meta_cond['doch.'+name] = self.get_argument(name)

            map(set_meta_cond_str, ['CT', 'LG'])

            # node conditions
            cond = {}
            if pos0 != '*':
                cond['pos.0'] = int(pos0)
            if pos1 != '*':
                cond['pos.1'] = int(pos1)
            if pos2 != '*':
                cond['pos.2'] = int(pos2)
            if pos3 != '*':
                cond['pos.3'] = int(pos3)
                
            if r0 != '*':
                cond['R.0'] = int(r0)
            if r1 != '*':
                cond['R.1'] = int(r1)
            if r2 != '*':
                cond['R.2'] = int(r2)
            if r3 != '*':
                cond['R.3'] = int(r3)

            def set_cond_str(name):
                if self.get_argument(name, None) is not None:
                    cond[name] = self.get_argument(name)

            def set_cond_int(name):
                if self.get_argument(name, None) is not None:
                    cond[name] = int(self.get_argument(name))

            map(set_cond_str, ['infoElement', 'infoValue', 'cateInfoElementName', 
                               'cateInfoElementValue', 'src', 'ref', 'L', 'keyword'])

            map(set_cond_int, ['defaultValue', 'cateIndex', 'cateSrcIndex'])

            stat, max_value = self.cond_stat(meta_cond, cond)
            self.render('_stat_result.html',
                        stat=stat, 
                        is_global=False,
                        max_value=max_value)
        else:
            self.compute_global_stat()
            stat_obj = self.global_stat()
            self.render('stat.html',
                        is_global=True,
                        stat=stat_obj['stat'], 
                        max_value=stat_obj['max_value'])


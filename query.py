# coding=utf-8

from tornado import ioloop, web, template, escape
from bson.objectid import ObjectId
import pymongo as M
import json, math, time

import base
from base import BaseRequestHandler


class DochHandler(BaseRequestHandler):
    def post(self):
        pass

class ShowTree(BaseRequestHandler):
    def get(self):
        self.post()

    def clean_node(self, node):
        node['_id'] = str(node.get('_id'))
        node['metaid'] = str(node['metaid'])
        node['children_ids'] = []
        for child in node['children']:
            self.clean_node(child)
    
    def post(self):
        metaid = self.get_argument('metaid')
        meta = self.db.meta.find_one({'_id': ObjectId(metaid)})
        
        print '----------------------------------------'
        self.clean_node(meta['tree'])

        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({'tree': meta['tree']}))

class ShowSrc(BaseRequestHandler):
    def get(self):
        self.post()
    
    def post(self):
        metaid = self.get_argument('metaid')
        meta = self.db.meta.find_one({'_id': ObjectId(metaid)})
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({'src': meta['srcDoc'],
                               'doch': meta['doch']}))

class ShowTaggedSrc(BaseRequestHandler):
    def get(self):
        self.post()

    def post(self):
        metaid = self.get_argument('metaid')
        meta = self.db.meta.find_one({'_id': ObjectId(metaid)})
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({'src': meta['taggedSrc'],
                               'doch': meta['doch'],
                               'vfile': meta.get('vfile')}))

class ShowHandler(BaseRequestHandler):
    def post(self):
        action = self.get_argument('action')
        metaid = self.get_argument('metaid')
        meta = self.db.meta.find_one({'_id': ObjectId(metaid)})
        if action == 'bodyxml':
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps({'xml': escape.xhtml_escape(meta['xmlDoc'])}))
        elif action == 'headxml':
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps({'xml': escape.xhtml_escape(meta['xmlDocH'])}))

class TextQueryHandler(BaseRequestHandler):
    def get(self):
        self.render('query.html')

    def post(self):
        self.render('query.html')

class QueryHandler(BaseRequestHandler):
    def get(self):
        self.render('query.html')

    def clean_node(self, node):
        node['_id'] = str(node['_id'])
        node['children_ids'] = map(str, node.get('children_ids', []))
        return node

    def record_query_history(self, meta_cond, node_cond):
        meta_cond_clone = dict(meta_cond)

        for k, v in meta_cond.iteritems():
            if k.find('.') > 0:
                del meta_cond_clone[k]
                meta_cond_clone[k.split('.')[1]] = v

        node_cond_clone = dict(node_cond)

        for k, v in node_cond.iteritems():
            if k.find('.') > 0:
                del node_cond_clone[k]
                node_cond_clone[k.split('.')[1]] = v

        self.db.history.insert(
            {'email': self.user['email'],
             'record':{
                    'meta_cond': meta_cond_clone,
                    'node_cond': node_cond_clone,
                    'time': int(time.time())}})

    def post(self):
        per_page = self.get_argument('perPage', '*')
        span_words = self.get_argument('spanWords', '*')
        pos0 = self.get_argument('pos0', '*')
        pos1 = self.get_argument('pos1', '*')
        pos2 = self.get_argument('pos2', '*')
        pos3 = self.get_argument('pos3', '*')
        info_element = self.get_argument('infoElement', '*')
        info_value = self.get_argument('infoValue', '*')
        default_value = self.get_argument('defaultValue', '*')
        src = self.get_argument('src', '*')
        ref = self.get_argument('ref', '*')
        l = self.get_argument('L', '*')
        r0 = self.get_argument('R0', '*')
        r1 = self.get_argument('R1', '*')
        r2 = self.get_argument('R2', '*')
        r3 = self.get_argument('R3', '*')
        keyword = self.get_argument('keyword', '*')
        cate_index = self.get_argument('cateIndex', '*')
        cate_src_index = self.get_argument('cateSrcIndex', '*')
        cate_info_element_name = self.get_argument('cateInfoElementName', '*')
        cate_info_element_value = self.get_argument('cateInfoElementValue', '*')

        sn = self.get_argument('SN', '*')
        lawyerA = self.get_argument('lawyerA', '*')
        lawyerB = self.get_argument('lawyerB', '*')
        lawyerC = self.get_argument('lawyerC', '*')
        judge = self.get_argument('judge', '*')
        interpreter = self.get_argument('interpreter', '*')
        partyA = self.get_argument('partyA', '*')
        partyB = self.get_argument('partyB', '*')
        partyC = self.get_argument('partyC', '*')
        testimonyA = self.get_argument('testimonyA', '*')
        testimonyB = self.get_argument('testimonyB', '*')
        testimonyC = self.get_argument('testimonyC', '*')
        andience = self.get_argument('audience', '*')
        authorwriter = self.get_argument('authorwriter', '*')
        quotesource = self.get_argument('quotesource', '*')
        recorder = self.get_argument('recorder', '*')

        meta_cond = {}
        def set_meta_cond_str(name):
            if self.get_argument(name, None) is not None:
                meta_cond['doch.'+name] = self.get_argument(name)

        map(set_meta_cond_str, ['SN', 'lawyerA', 'lawyerB', 'lawyerC',
                                'judge', 'interpreter', 'partyA', 'partyB', 'partyC',
                                'testimonyA', 'testimonyB', 'testimonyC',
                                'audience', 'authorwriter', 'quotesource', 'recorder',
                                'CT', 'LG'])

        meta_cond['status'] = base.MS_PASSED
        meta_cond['secret'] = {'$lte': self.secret_level()}

        meta_cursor = self.db.meta.find(meta_cond)

        cond_str = '['+pos0+','+pos1+','+pos2+','+pos3+'],'+\
            info_element+','+info_value+','+\
            default_value+','+src+','+\
            ref+','+l+',['+r0+','+r1+','+r2+','+r3+'],'+keyword

        cond = {}

        if pos0 != '*':
            cond['pos.0'] = int(pos0)
        if pos1 != '*':
            cond['pos.1'] = int(pos1)
        if pos2 != '*':
            cond['pos.2'] = int(pos2)
        if pos3 != '*':
            cond['pos.3'] = int(pos3)

        if r0 != '*':
            cond['R.0'] = int(r0)
        if r1 != '*':
            cond['R.1'] = int(r1)
        if r2 != '*':
            cond['R.2'] = int(r2)
        if r3 != '*':
            cond['R.3'] = int(r3)

        def set_cond_str(name):
            if self.get_argument(name, None) is not None:
                cond[name] = self.get_argument(name)

        def set_cond_int(name):
            if self.get_argument(name, None) is not None:
                cond[name] = int(self.get_argument(name))

        map(set_cond_str, ['infoElement', 'infoValue', 'cateInfoElementName', 
                           'cateInfoElementValue', 'src', 'ref', 'L', 'keyword'])

        map(set_cond_int, ['defaultValue', 'cateIndex', 'cateSrcIndex'])

        nodes = []

        for meta_obj in meta_cursor:
            cond['metaid'] = str(meta_obj['_id'])
            cursor = self.db.nodes.find(cond)
            print cursor.count()
            for node in cursor:
                left_cursor = self.db.nodes.find({'nodeIndex': 
                                                  {'$gt': node['nodeIndex']-int(span_words),
                                                   '$lt': node['nodeIndex']},
                                                  'metaid': node['metaid']}).sort('nodeIndex', M.ASCENDING)
                right_cursor = self.db.nodes.find({'nodeIndex': 
                                                   {'$lt': node['nodeIndex']+int(span_words),
                                                    '$gt': node['nodeIndex']},
                                                   'metaid': node['metaid']}).sort('nodeIndex', M.ASCENDING)

                node = self.clean_node(node)
                node['leftNodes'] = [self.clean_node(n) for n in left_cursor]
                node['rightNodes'] = [self.clean_node(n) for n in right_cursor]

                nodes.append(node)

        node_count = len(nodes)

        page = int(self.get_argument('page'))
        limit = int(per_page)
        start = page * limit
        end = limit + start

        start = node_count if start > node_count else start
        end = node_count if end > node_count else end

        nodes = nodes[start:end]

        if not self.is_guest():
            meta_cond['secret']['<='] = meta_cond['secret']['$lte']
            del meta_cond['secret']['$lte']
            self.record_query_history(meta_cond, cond)
        
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps({'nodes': nodes,
                               'node_count': node_count,
                               'page_count': math.ceil(float(node_count)/float(per_page)),
                               'start_index': start+1}))

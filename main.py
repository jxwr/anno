# encoding=utf8
from tornado import ioloop, web, template
from bson.objectid import ObjectId
import pymongo as M
import json

import os, string, random, time

from config import STATIC_PATH
from base import BaseRequestHandler, MS_PENDING, MS_PASSED, MS_REFUSED, MS_DRAFT
from query import QueryHandler, TextQueryHandler, ShowTaggedSrc, ShowSrc, ShowTree, ShowHandler
from statistics import StatHandler
from account import LoginHandler, LogoutHandler, RegisterHandler

from personal import FavHandler, HistoryHandler, MineHandler
from admin import AdminPendingHandler, AdminUserHandler, AdminCorpusHandler

class StorageHandler(BaseRequestHandler):
    def store_meta(self, metaid, metadata):
        metadata['status'] = MS_PENDING
        self.db.meta.update({'_id': ObjectId(metaid)}, {'$set': metadata})

    def _store_tree(self, metaid, node):
        node['metaid'] = metaid
#        print 'children', node['children']
        if len(node['children']) == 0:
            return self.db.nodes.insert(node)
        else:
            node['children_ids'] = []
            for child in node['children']:
                node['children_ids'].append(self._store_tree(metaid, child))
            
            # copy
            n = dict(node)
            n['children'] = None
            return self.db.nodes.insert(n)

    def store_tree(self, metaid, tree):
        return self._store_tree(metaid, tree)

    @web.authenticated
    def get(self):
        self.post()

    def post(self):
        self.set_header('Content-Type', 'application/json')

        req = self.request
        metaid = self.get_argument('metaid', False)
        if not metaid:
            return self.write('false')
        anno_tree = json.loads(self.get_argument('annoTree'))

        doch = anno_tree['doch'];
        doch_long = anno_tree['dochLong'];
        xml_doc = anno_tree['xmlDoc'];
        xml_doch = anno_tree['xmlDocH'];
        tagged_src = anno_tree['taggedSrc'];
        tree = anno_tree['tree'];

        # clear nodes of the doc
        self.db.nodes.remove({'metaid': metaid})
        # generate nodes again
        root_id = self.store_tree(metaid, dict(tree))

        self.store_meta(metaid,
                        {'doch': doch,
                         'dochLong': doch_long,
                         'xmlDoc': xml_doc,
                         'xmlDocH': xml_doch,
                         'taggedSrc': tagged_src,
                         'tree': tree,
                         'rootId': root_id,
                         'upload_time': time.time()})
        
        self.set_header('Content-Type', 'application/json')
        self.write(self.get_argument('annoTree'))

        self.session['metaid'] = None
        self.session['vfile'] = None

class HelpHandler(BaseRequestHandler):
    def get(self):
        self.render('help.html')

class AnnoHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        metaid = self.session.get('metaid', False)
        vfile = self.session.get('vfile', False)
        meta = {}

        if metaid:
            meta = self.db.meta.find_one({'_id': metaid}) or {}
            if not meta:
                self.session['metaid'] = None
                meta = {}
                metaid = 0
                vfile = ''

        self.render('tags.html', metaid=metaid, meta=meta, vfile=vfile)

class UploadHandler(BaseRequestHandler):
    @web.authenticated
    def post(self):
        try:
            srcfile = self.request.files['srcfile'][0]
            
            filename = srcfile['filename']
            body = srcfile['body']

            try:
                body = body.decode('GBK', 'strict')
            except UnicodeError:
                pass

            metaid = self.db.meta.insert({'ownerID': self.user['_id'],
                                          'status': MS_DRAFT,
                                          'srcDocName':filename,
                                          'srcDoc': body,
                                          'upload_time': time.time(),
                                          'taggedSrc': body})
            self.session['metaid'] = metaid

            if self.request.files.get('vfile'):
                vfile = self.request.files.get('vfile')[0]
                original_fname = vfile['filename']
                extension = os.path.splitext(original_fname)[1]
                fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))+str(long(time.time()))
                final_filename= fname+extension
                output_file = open(STATIC_PATH+"/video/" + final_filename, 'w')
                output_file.write(vfile['body'])

                self.db.meta.update({'_id': ObjectId(metaid)}, {'$set': {'vfile': final_filename}})
                self.session['vfile'] = final_filename
        except Exception, e:
            print e
            
        self.redirect('/personal/anno')

class VideoHandler(BaseRequestHandler):
    def get(self):
        self.render('flash.html')

    @web.authenticated
    def post(self):
        import os, string, random, time
        vfile = None
        
        if self.request.files.get('vfile', False):
            vfile = self.request.files['vfile'][0]
        else:
            self.render(u'没有视频文件')
            return

        original_fname = vfile['filename']
        extension = os.path.splitext(original_fname)[1]
        fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(6))+str(long(time.time()))
        final_filename= fname+extension
        output_file = open(STATIC_PATH+"/video/" + final_filename, 'w')
        output_file.write(vfile['body'])

        metaid = self.session.get('metaid', False)

        self.db.meta.update({'_id': ObjectId(metaid)}, {'$set': {'vfile': final_filename}})
        
        self.session['vfile'] = final_filename
        self.redirect('/personal/anno')

def run():
    loader = template.Loader('templates')
    application = web.Application(
        [
            (r"/", LoginHandler),
            (r"/account/login", LoginHandler),
            (r"/account/logout", LogoutHandler),
            (r"/account/register", RegisterHandler),
            (r"/personal/fav", FavHandler),
            (r"/personal/history", HistoryHandler),
            (r"/personal/anno", AnnoHandler),
            (r"/personal/mine", MineHandler),
            (r"/admin/pending", AdminPendingHandler),
            (r"/admin/user", AdminUserHandler),
            (r"/admin/corpus", AdminCorpusHandler),
            (r"/stat", StatHandler),
            (r"/video", VideoHandler),
            (r"/store", StorageHandler),
            (r"/upload", UploadHandler),
            (r"/query", QueryHandler),
            (r"/query/text", TextQueryHandler),
            (r"/show", ShowHandler),
            (r"/show/tree", ShowTree),
            (r"/show/src", ShowSrc),
            (r"/show/taggedsrc", ShowTaggedSrc),
            (r"/help", HelpHandler),
            (r"/static/(.*)", web.StaticFileHandler, {"path": STATIC_PATH}),
        ],
        debug=True)
    application.listen(8888)
    ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    run()

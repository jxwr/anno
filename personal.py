# coding=utf-8

from tornado import ioloop, web, template
from bson.objectid import ObjectId
import pymongo as M
import time, json

import base
from base import BaseRequestHandler, PRIO_ADMIN, PRIO_USER, PRIO_GUEST

string_map = {
    'status': u'（内部状态）',
    'pos_0': u'父x',
    'pos_1': u'父y',
    'pos_2': u'子x',
    'pos_3': u'子y',
    'infoElement': u'信息元素',
    'infoValue': u'信息点值',
    'defaultValue': u'默认值',
    'src': u'信息源',
    'ref': u'被引用者',
    'L': u'L信息',
    'R_0': u'R[0]',
    'R_1': u'R[1]',
    'R_2': u'R[2]',
    'R_3': u'R[3]',
    'keyword': u'关键字',
    'cateIndex': u'Cate编号',
    'cateSrcIndex': u'Cate源序号',
    'cateInfoElementName': u'Cate信息元素名',
    'cateInfoElementValue': u'Cate信息元素值',
    'SN': u'序列号',
    'T': u'标题',
    'Ku': u'Ke关键字',
    'Ke': u'核心命题',
    'CT': u'案件类型',
    'doch_CT': u'案件类型',
    'LG': u'语言',
    'doch_LG': u'语言',
    'ST': u'源文/目标文',
    'lawyerA': u'律师A',
    'lawyerB': u'律师B',
    'lawyerC': u'律师C',
    'judge': u'法官',
    'interpreter': u'口译员',
    'partyA': u'当事人A',
    'partyB': u'当事人B',
    'partyC': u'当事人C',
    'testimonyA': u'证人A',
    'testimonyB': u'证人B',
    'testimonyC': u'证人C',
    'audience': u'观众',
    'authorwriter': u'作者',
    'quotesource': u'引用源',
    'recorder': u'书记员',
    'secret': u'密级',
    'metaid': u'ID'
}

class FavHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        cursor = self.db.fav.find({'email': self.user['email']})
        doc_meta_list = []

        for obj in cursor:
            meta = self.db.meta.find_one({'_id': ObjectId(obj['metaid'])})
            if meta:
                meta['time'] = obj['time']
                doc_meta_list.append(meta)
        
        self.render('fav.html', meta_list=doc_meta_list)

    @web.authenticated
    def post(self):
        self.set_header('Content-Type', 'application/json')

        metaid = self.get_argument('metaid', False)
        if metaid is False:
            self.write(json.dumps({'result': False}))
        else:
            fav = self.db.fav.find_one({'email': self.user['email'],
                                   'metaid': metaid})
            if fav:
                return self.write(json.dumps({'result': True}))

            self.db.fav.insert({'email': self.user['email'],
                                'metaid': metaid,
                                'time': int(time.time())
                                })
            self.write(json.dumps({'result': True}))

class HistoryHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        cursor = self.db.history.find({'email': self.user['email']})
        record_list = []

        for obj in cursor:
            record_list.append(obj['record'])

        record_list.reverse()
        
        self.render('history.html', record_list=record_list, string_map=string_map)

class MineHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        mine_list = self.db.meta.find({'ownerID': self.user['_id']})

        self.render('mine.html', meta_list=mine_list, base=base)
        
    @web.authenticated
    def post(self):
        action = self.get_argument('action')
        if action == 'edit':
            metaid = self.get_argument('metaid')
            self.session['metaid'] = ObjectId(metaid)
            meta = self.db.meta.find_one({'_id': ObjectId(metaid)})
            self.session['vfile'] = meta.get('vfile')
            self.write('ok')
        elif action == 'draft':
            metaid = self.session['metaid']
            draft = self.get_argument('body_draft')
            tree_draft = self.get_argument('tree_draft')
            tree_draft = json.loads(tree_draft)

            self.db.meta.update({'_id': ObjectId(metaid)}, 
                                {'$set': {'taggedSrc': draft,
                                          'status': base.MS_DRAFT,
                                          'tree_draft': tree_draft}})
        elif action == 'delete':
            metaid = self.get_argument('metaid')
            self.db.meta.remove({'_id': ObjectId(metaid)})

# coding=utf-8

from bson.objectid import ObjectId
import pymongo as M
import time

from base import BaseRequestHandler, PRIO_ADMIN, PRIO_USER, PRIO_GUEST

class LoginHandler(BaseRequestHandler):
    def get(self):
        user = self.session.get('user')
        if user is not None:
            self.redirect('/query')
        else:
            self.render('login.html', message='')

    def post(self):
        email = self.get_argument('email')
        password = self.get_argument('password')

        user = self.db.user.find_one({'email': email, 'password': password})
        if user is None:
            self.render('login.html', message=u'邮箱或密码错误')
        else:
            self.remember(user)
            self.set_cookie('email', user['email'])
            self.set_cookie('token', user['token'])
            self.redirect(self.get_argument('next', '/'))

class LogoutHandler(BaseRequestHandler):
    def get(self):
        self.post()

    def post(self):
        if self.session.get('user', False):
            self.session['user'] = None
            self.session['metaid'] = None
            self.session['vfile'] = None
            self.redirect('/')

class RegisterHandler(BaseRequestHandler):
    def get(self):
        self.render('register.html', success=False, message='')

    def post(self):
        name = self.get_argument('name')
        email = self.get_argument('email')
        password = self.get_argument('password')

        user = self.db.user.find_one({'email': email})

        if user is None:
            success = True
            message = u''
            if len(name) > 20:
                message = u'名字长度过长'
                success = False
            elif email.find('@') < 0:
                message = u'邮箱格式不正确'
                success = False
            elif len(password) < 6:
                message = u'密码长度不正确'
                success = False

            self.db.user.insert({'name': name,
                                 'email': email,
                                 'password': password,
                                 'score': 0,
                                 'token': str(int(time.time()*time.time()*11)),
                                 'prio': PRIO_USER})
            self.render('register.html', success=success, message=message)
        else:
            self.render('register.html', success=False, message=u'邮箱已被注册')

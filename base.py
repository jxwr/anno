from tornado import web
import pymongo as M
from config import TEMPLATE_PATH
from session import MongoDBSession

PRIO_ADMIN = 1
PRIO_USER  = 2
PRIO_GUEST = 3

MS_PENDING = 1
MS_PASSED  = 2
MS_REFUSED = 3 
MS_DRAFT   = 4 

class BaseRequestHandler(web.RequestHandler):
    def prepare(self):
        self.PRIO_ADMIN = 1
        self.PRIO_USER = 2
        self.PRIO_GUEST = 3

        self.connection = M.Connection()
        self.db = self.connection.anno
        session_id = self.get_cookie('session_id', None)

        if session_id is not None:
            self.session = MongoDBSession.load(session_id, self.db.tornado_sessions)
        if session_id is None or self.session is None:
            self.session = MongoDBSession(self.db.tornado_sessions)
            self.set_cookie('session_id', self.session.session_id)
        
        user = self.session.get('user', False)
        if not user:
            self.user = None
            self.prio = PRIO_GUEST
        else:
            self.user = user
            self.prio = user['prio']
            self.name = user['name']

    def is_guest(self):
        return self.prio == PRIO_GUEST

    def is_admin(self):
        return self.prio == PRIO_ADMIN

    def is_user(self):
        return self.prio == PRIO_USER

    def secret_level(self):
        if self.is_guest():
            return 0
        elif self.is_user():
            score = self.user.get('score', 0)
            return score/100
        elif self.is_admin():
            return 10

    def remember(self, user):
        self.session['user'] = user
        self.user = user
        self.prio = user['prio']
        self.name = user['name']

    def get_current_user(self):
        return self.user

    def on_finish(self):
        self.session.save()

    def get_template_path(self):
        return TEMPLATE_PATH

    def get_login_url(self):
        return '/account/login'

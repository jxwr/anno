# coding=utf-8

from tornado import ioloop, web, template
from bson.objectid import ObjectId
import pymongo as M
import json, math, time

import base
from base import BaseRequestHandler, PRIO_ADMIN, PRIO_USER

class AdminUserHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        if not self.is_admin():
            return

        admin_cursor = self.db.user.find({'prio': PRIO_ADMIN})
        user_cursor = self.db.user.find({'prio': PRIO_USER})

        self.render('admin_user.html', 
                    admin_list=[a for a in admin_cursor], 
                    user_list=[a for a in user_cursor], 
                    user=self.get_current_user())

    @web.authenticated
    def post(self):
        if not self.is_admin():
            return

        if self.get_argument('action') is not None:
            email = self.get_argument('email')
            self.db.user.remove({'email': email})
            self.write('true')
        else:
            self.write('false')

class AdminPendingHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        if not self.is_admin():
            return

        cursor = self.db.meta.find({'status': base.MS_PENDING})
        doc_meta_list = []

        for meta in cursor:
            doc_meta_list.append(meta)

        cursor = self.db.corpus.find()
        corpus_list = []

        for corpus in cursor:
            corpus_list.append(corpus)

        self.render('admin.html', 
                    meta_list=doc_meta_list,
                    corpus_list=corpus_list)

    @web.authenticated
    def post(self):
        if not self.is_admin():
            return

        metaid = self.get_argument('metaid', False)
        is_pass = self.get_argument('pass', 'false')
        status = base.MS_PASSED if is_pass == 'true' else base.MS_REFUSED

        if status == base.MS_PASSED: 
            status = base.MS_PENDING

            corpus = self.get_argument('corpus')
            secret = int(self.get_argument('secret', 1))
            expire = int(self.get_argument('expire', 100))
            score = int(self.get_argument('score', 10))

            meta_obj = self.db.meta.find_one({'_id': ObjectId(metaid)})
            ownerID = meta_obj['ownerID']

            obj = self.db.user.find_one({'_id': ownerID})
            old_score = int(obj.get('score', 0)) if obj else 0

            self.db.user.update({'_id': ownerID},
                                {'$set': {'score': old_score + score}})

            self.db.meta.update({'_id': ObjectId(metaid)}, 
                                {'$set': {'status': status,
                                          'corpus': corpus,
                                          'secret': secret,
                                          'expire_years': expire,
                                          'pass_time': time.time()}})
        elif status == base.MS_REFUSED:
            self.db.meta.update({'_id': ObjectId(metaid)}, 
                                {'$set': {'status': status}})
            
        self.write('ok')


class AdminCorpusHandler(BaseRequestHandler):
    @web.authenticated
    def get(self):
        if not self.is_admin():
            return

        cursor = self.db.corpus.find()
        corpus_list = []

        for corpus in cursor:
            corpus_list.append(corpus)
        
        self.render('admin_corpus.html', corpus_list=corpus_list)

    @web.authenticated
    def post(self):
        if not self.is_admin():
            return

        action = self.get_argument('action')
        if action == 'add':
            corpus = self.get_argument('corpus')
            self.db.corpus.insert({'name': corpus})
        elif action == 'delete':
            cid = self.get_argument('cid')
            self.db.corpus.remove({'_id': ObjectId(cid)})

        self.get()

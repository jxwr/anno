import main
import os


if __name__ == '__main__':
    newpid = os.fork()
    if newpid == 0:
        main.run()
    else:
        print "started as daemon"
